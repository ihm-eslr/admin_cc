package fr.unice.polytech.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Locale;

public class ChartController {

    @FXML
    private BarChart<String, Integer> chart;

    @FXML
    private CategoryAxis Xaxis;

    private ObservableList<String> month = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        String[] months = DateFormatSymbols.getInstance(Locale.FRENCH).getMonths();
        month.addAll(Arrays.asList(months));
        Xaxis.setCategories(month);
        loadData();
    }

    /**
     * Sets the persons to show the statistics for.
     *
     * @param
     */
    public void loadData() {
        int[] j = new int[12];
        for (int i = 0; i < 12; i++) {
            j[i] = i;
        }

        XYChart.Series<String, Integer> series = createDataSeries(j);
        chart.getData().add(series);
    }


    private XYChart.Series<String, Integer> createDataSeries(int[] monthCounter) {
        XYChart.Series<String,Integer> series = new XYChart.Series<String,Integer>();

        for (int i = 0; i < monthCounter.length; i++) {
            XYChart.Data<String, Integer> monthData = new XYChart.Data<String,Integer>(month.get(i), monthCounter[i]);
            series.getData().add(monthData);
        }

        return series;
    }

}
