package fr.unice.polytech.controller;


import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by florian on 15/03/2017.
 */
public class BottomBarController {

    @FXML
    private Circle bottomBar;

    @FXML
    public void displayChart(MouseEvent event) throws IOException {
        changePaneView("/fxml/Chart.fxml");
    }

    @FXML
    public void displayEvents(MouseEvent event) throws IOException {
        changePaneView("/fxml/admin_Events.fxml");
    }

    @FXML
    public void displayProducts(MouseEvent event) throws IOException {
        changePaneView("/fxml/admin_products.fxml");
    }

    @FXML
    public void initialize() {

    }

    private void changePaneView(String path) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
        Stage stage = (Stage) bottomBar.getScene().getWindow();
        Parent rootNode = loader.load(getClass().getResourceAsStream(path));
        Scene scene = new Scene(rootNode);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void displayShops(MouseEvent mouseEvent) throws IOException {
        changePaneView("/fxml/admin_shops.fxml");
    }
}

/*package fr.unice.polytech.controller;
        import javafx.fxml.FXML;
        import javafx.scene.input.MouseEvent;
        import javafx.scene.shape.Circle;

public class BottomBarController {

    @FXML
    private Circle bottomBar;

    @FXML
    void displayShops(MouseEvent event) {

    }

}*/