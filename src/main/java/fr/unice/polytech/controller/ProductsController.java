package fr.unice.polytech.controller;

import fr.unice.polytech.dao.DAOProduct;
import fr.unice.polytech.model.Product;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.util.List;

/**
 * Created by seb on 14/03/17.
 */
public class ProductsController {

    @FXML
    private TableView<Product> tableView;

    @FXML
    private TableColumn<Product, Integer> idProduct;

    @FXML
    private TableColumn<Product, Integer> idShop;

    @FXML
    private TableColumn<Product, String> name;

    @FXML
    private TableColumn<Product, Integer> price;

    @FXML
    private TableColumn<Product, String> image;

    @FXML
    private ImageView addButton;

    @FXML
    private ImageView deleteButton;

    @FXML
    private ImageView checkButton;

    @FXML
    void onAddClick(MouseEvent event) {

    }

    @FXML
    void onCheckClick(MouseEvent event) {

    }

    @FXML
    void onDelClick(MouseEvent event) {

    }

    @FXML
    public void initialize(){
        addButton.setImage(new Image("/images/plus.png"));
        deleteButton.setImage(new Image("/images/moins.png"));
        checkButton.setImage(new Image("/images/check.png"));


        ObservableList<Product> products= new DAOProduct().getProducts();

        idProduct.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());

        name.setCellValueFactory(cellData -> cellData.getValue().getName());


        image.setCellValueFactory(cellData -> cellData.getValue().getImage());
        image.setCellFactory(TextFieldTableCell.forTableColumn());
        image.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Product, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Product, String> t) {
                        ((Product) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setImage(t.getNewValue());
                    }
                }
        );



        idShop.setCellValueFactory(cellData -> cellData.getValue().idShopProperty().asObject());
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Product, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Product, String> t) {
                        ((Product) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setName(t.getNewValue());
                    }
                }
        );

        tableView.setItems(products);
        tableView.setEditable(true);
    }

    @FXML
    public void edit(TableColumn.CellEditEvent<Product, Integer> productIntegerCellEditEvent) {

    }
}
