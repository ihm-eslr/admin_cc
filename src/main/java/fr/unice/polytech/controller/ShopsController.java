package fr.unice.polytech.controller;

import fr.unice.polytech.dao.DAOShop;
import fr.unice.polytech.model.Product;
import fr.unice.polytech.model.Shop;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Created by seb on 14/03/17.
 */
public class ShopsController {

    @FXML
    private TableView<Shop> tableView;

    @FXML
    private TableColumn<Shop, Integer> idShop;

    @FXML
    private TableColumn<Shop, String> name;

    @FXML
    private TableColumn<Shop, String> enDescription;

    @FXML
    private TableColumn<Shop, String> frDescription;

    @FXML
    private TableColumn<Shop, String> logo;

    @FXML
    private TableColumn<Shop, String> plan;

    @FXML
    private TableColumn<Shop, String> catégories;

    @FXML
    private ImageView addButton;

    @FXML
    private ImageView deleteButton;

    @FXML
    private ImageView checkButton;

    @FXML
    void onAddClick(MouseEvent event) {
    }

    @FXML
    void onCheckClick(MouseEvent event) {

    }

    @FXML
    void onDelClick(MouseEvent event) {

    }

    @FXML
    public void initialize(){
        addButton.setImage(new Image("/images/plus.png"));
        deleteButton.setImage(new Image("/images/moins.png"));
        checkButton.setImage(new Image("/images/check.png"));


        ObservableList<Shop> shops= new DAOShop().getShop();
        idShop.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());

        name.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Shop, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Shop, String> t) {
                        ((Shop) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setName(t.getNewValue());
                    }
                }
        );

        enDescription.setCellValueFactory(cellData -> cellData.getValue().englishDescriptionProperty());
        enDescription.setCellFactory(TextFieldTableCell.forTableColumn());
        enDescription.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Shop, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Shop, String> t) {
                        ((Shop) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setDescriptionEN(t.getNewValue());
                    }
                }
        );

        frDescription.setCellValueFactory(cellData -> cellData.getValue().frenchDescriptionProperty());
        frDescription.setCellFactory(TextFieldTableCell.forTableColumn());
        frDescription.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Shop, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Shop, String> t) {
                        ((Shop) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setDescriptionEN(t.getNewValue());
                    }
                }
        );

        logo.setCellValueFactory(cellData -> cellData.getValue().getLogoProperty());
        logo.setCellFactory(TextFieldTableCell.forTableColumn());
        logo.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Shop, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Shop, String> t) {
                        ((Shop) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setLogo(t.getNewValue());
                    }
                }
        );

        plan.setCellValueFactory(cellData -> cellData.getValue().getMapProperty());
        plan.setCellFactory(TextFieldTableCell.forTableColumn());
        plan.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Shop, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Shop, String> t) {
                        ((Shop) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setPlan(t.getNewValue());
                    }
                }
        );

        catégories.setCellValueFactory(cellData -> cellData.getValue().getCategorieProprty());



        tableView.setEditable(true);
        tableView.setItems(shops);

    }

    private void displayDataBaseShop(){
    }


}
