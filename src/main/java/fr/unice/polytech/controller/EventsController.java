package fr.unice.polytech.controller;

import fr.unice.polytech.dao.DAOEvent;
import fr.unice.polytech.dao.DAOShop;
import fr.unice.polytech.model.Event;
import fr.unice.polytech.model.Shop;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Created by seb on 14/03/17.
 */
public class EventsController {

    @FXML
    private TableView<Event> tableView;

    @FXML
    private TableColumn<Event, String> name;

    @FXML
    private TableColumn<Event, Integer> idShop;

    @FXML
    private TableColumn<Event, String> image;

    @FXML
    private ImageView addButton;

    @FXML
    private ImageView deleteButton;

    @FXML
    private ImageView checkButton;

    @FXML
    void onAddClick(MouseEvent event) {

    }

    @FXML
    void onCheckClick(MouseEvent event) {

    }

    @FXML
    void onDelClick(MouseEvent event) {

    }

    @FXML
    public void initialize(){
        addButton.setImage(new Image("/images/plus.png"));
        deleteButton.setImage(new Image("/images/moins.png"));
        checkButton.setImage(new Image("/images/check.png"));


        ObservableList<Event> events= new DAOEvent().getEvents();
        idShop.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        name.setCellValueFactory(cellData -> cellData.getValue().getName());

        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Event, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Event, String> t) {
                        ((Event) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setName(t.getNewValue());
                    }
                }
        );

        image.setCellValueFactory(cellData -> cellData.getValue().getImage());
        image.setCellFactory(TextFieldTableCell.forTableColumn());
        image.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Event, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Event, String> t) {
                        ((Event) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setName(t.getNewValue());
                    }
                }
        );

        tableView.setEditable(true);
        tableView.setItems(events);
    }
}
