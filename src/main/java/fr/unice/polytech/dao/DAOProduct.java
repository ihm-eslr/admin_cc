package fr.unice.polytech.dao;

import fr.unice.polytech.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by florian on 11/03/2017.
 */
public class DAOProduct {

    private ObservableList<Product> products;

    public DAOProduct() {
        products = FXCollections.observableArrayList();
    }

    private void requireProducts() {

        try {
            //ICI
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dev", "dev", "");
            //A REMPLACER les deux dev par ton nom

            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM products;" );

            while ( resultSet.next() ) {

                products.add(
                        new Product(
                                resultSet.getInt("IDProduct"),
                                resultSet.getInt("IDShop"),
                                resultSet.getString("Name"),
                                resultSet.getString("Image"),
                                resultSet.getInt("Price")
                        )
                );

            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch ( Exception e ) {
            System.out.println("Unable to connect to the database");
        }

    }

    public ObservableList<Product> getProducts() {
        requireProducts();
        return products;
    }


}
