package fr.unice.polytech.dao;

import fr.unice.polytech.model.Event;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by florian on 11/03/2017.
 */
public class DAOEvent {

    private ObservableList<Event> events;

    public DAOEvent() {
        events = FXCollections.observableArrayList();
    }

    private void requireEvents() {

        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dev", "dev", "");

            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM events;" );

            while ( resultSet.next() ) {

                events.add(
                        new Event(
                                resultSet.getInt("IDShop"),
                                resultSet.getString("Name"),
                                resultSet.getString("Image")
                        )
                );

            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            System.out.println("Unable to connect to the database");
        }

    }

    public ObservableList<Event> getEvents() {
        requireEvents();
        return events;
    }
}
