package fr.unice.polytech.dao;

import fr.unice.polytech.model.Shop;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by florian on 11/03/2017.
 */
public class DAOShop {

    private ObservableList<Shop> shops;

    public DAOShop() {
        shops = FXCollections.observableArrayList();
    }

    private void requireShops() {

        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dev", "dev", "");

            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM shops;" );

            while ( resultSet.next() ) {

                shops.add(
                    new Shop(
                        resultSet.getInt("IDShop"),
                        resultSet.getString("Name"),
                        resultSet.getString("English_Description"),
                        resultSet.getString("French_Description"),
                        resultSet.getString("Logo"),
                        resultSet.getString("Map"),
                        resultSet.getString("Category")
                    )
                );

            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch ( Exception e ) {
            System.out.println("Unable to connect to the database");
        }

    }

    public ObservableList<Shop> getShop() {
        requireShops();
        return shops;
    }


}
