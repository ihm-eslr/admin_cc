package fr.unice.polytech.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by florian on 08/03/2017.
 */
public class Shop {

    /**
     * attributes
     */
    private IntegerProperty id;
    private StringProperty name;
    private StringProperty englishDescription;
    private StringProperty frenchDescription;
    private StringProperty logo;
    private StringProperty map;
    private StringProperty categorie;

    /**
     * constructor
     *
     * @param id
     * @param name
     * @param englishDescription
     * @param frenchDescription
     * @param logo
     */
    public Shop(int id, String name, String englishDescription, String frenchDescription, String logo, String map, String categorie) {
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.englishDescription = new SimpleStringProperty(englishDescription);
        this.frenchDescription=new SimpleStringProperty(frenchDescription);
        this.logo = new SimpleStringProperty(logo);
        this.map = new SimpleStringProperty(map);
        this.categorie = new SimpleStringProperty(categorie);
    }
    //TODO categorie n'est pas INIT

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getEnglishDescription() {
        return englishDescription.get();
    }

    public String getFrenchDescription(){ return frenchDescription.get();}

    public StringProperty englishDescriptionProperty() {
        return englishDescription;
    }

    public StringProperty frenchDescriptionProperty(){return frenchDescription;}

    public StringProperty getMapProperty(){return map;}

    //TODO en attendant ENUM
    public StringProperty getCategorieProprty(){return categorie;}

    public StringProperty getLogoProperty(){
        return logo;
    }

    public void setName(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public void setDescriptionEN(String descriptionEN) {
        this.englishDescription = new SimpleStringProperty(descriptionEN);
    }

    public void setDescriptionFR(String descriptionFR) {
        this.frenchDescription = new SimpleStringProperty(descriptionFR);
    }

    public void setLogo(String logo) {
        this.logo = new SimpleStringProperty(logo);
    }

    public void setPlan(String map) {
        this.map = new SimpleStringProperty(map);
    }
}
