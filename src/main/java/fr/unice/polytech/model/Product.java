package fr.unice.polytech.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by florian on 08/03/2017.
 */
public class Product {

    /**
     * attributes
     */
    private IntegerProperty id;
    private IntegerProperty idShop;
    private StringProperty name;
    private IntegerProperty price;
    private StringProperty image;

    /**
     * constructor
     * @param name
     * @param image
     * @param price
     */
    public Product(int id, int idShop, String name, String image , int price){
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.image = new SimpleStringProperty(image);
        this.price = new SimpleIntegerProperty(price);
        this.idShop = new SimpleIntegerProperty(idShop);

    }

    public StringProperty getImage(){
        return image;
    }

    public StringProperty getName(){return name;}

    public IntegerProperty idProperty() {
        return id;
    }

    public int getIdShop() {
        return idShop.get();
    }

    public IntegerProperty idShopProperty() {
        return idShop;
    }

    public void setName(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public void setImage(String image) {
        this.image = new SimpleStringProperty(image);
    }

    public void setPrice(int price) {
        this.price.set(price);
    }

    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public void setIdShop(int id) {
        this.idShop = new SimpleIntegerProperty(id);
    }
}
