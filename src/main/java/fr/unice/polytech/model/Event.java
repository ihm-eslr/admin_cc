package fr.unice.polytech.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by florian on 08/03/2017.
 */
public class Event {

    /**
     * attributes
     */
    private IntegerProperty id;
    private StringProperty name;
    private StringProperty image;

    /**
     * constructor
     * @param name
     * @param image
     */
    public Event(int id, String name, String image) {
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.image = new SimpleStringProperty(image);
    }

    public StringProperty getImage() {
        return image;
    }

    public StringProperty getName() {
        return name;
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setName(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public void setImage(String image) {
        this.image = new SimpleStringProperty(image);
    }
}

